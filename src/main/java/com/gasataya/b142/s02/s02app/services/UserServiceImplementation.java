package com.gasataya.b142.s02.s02app.services;

import com.gasataya.b142.s02.s02app.models.User;
import com.gasataya.b142.s02.s02app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImplementation implements UserService {
    @Autowired
    private UserRepository userRepository;

    public void createUser(User newUser) {
        userRepository.save(newUser);
    }

    public void updateUser(Long id, User updatedUser) {
        User existingUser = userRepository.findById(id).get();
        existingUser.setPassword(updatedUser.getPassword());
        existingUser.setUsername(updatedUser.getUsername());
        userRepository.save(existingUser);
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }
}
